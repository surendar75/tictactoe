pub mod board;

pub use crate::board::Board;

#[test]
fn test_winner() {
    let board = Board {
        board: [
            [Some('O'), Some('X'), Some('X')],
            [Some('X'), Some('O'), Some('O')],
            [Some('O'), Some('X'), Some('X')],
        ],
    };

    assert_eq!(board.winner(), None);

    // Testing rows
    let board = Board {
        board: [[None; 3], [None; 3], [Some('X'); 3]],
    };
    assert_eq!(board.winner(), Some('X'));
    let board = Board {
        board: [[None; 3], [Some('O'); 3], [None; 3]],
    };
    assert_eq!(board.winner(), Some('O'));
    let board = Board {
        board: [[Some('X'); 3], [None; 3], [None; 3]],
    };
    assert_eq!(board.winner(), Some('X'));

    //Testing diagonals
    let board = Board {
        board: [
            [Some('X'), None, None],
            [None, Some('X'), None],
            [None, None, Some('X')],
        ],
    };
    assert_eq!(board.winner(), Some('X'));

    let board = Board {
        board: [
            [None, None, Some('O')],
            [None, Some('O'), None],
            [Some('O'), None, None],
        ],
    };

    assert_eq!(board.winner(), Some('O'));

    // Testing Columns
    let board = Board {
        board: [
            [Some('X'), Some('O'), Some('O')],
            [Some('O'), Some('O'), Some('X')],
            [Some('X'), Some('O'), Some('X')],
        ],
    };

    assert_eq!(board.winner(), Some('O'));

    let board = Board {
        board: [
            [Some('X'), None, None],
            [Some('X'), Some('O'), Some('O')],
            [Some('X'), None, None],
        ],
    };

    assert_eq!(board.winner(), Some('X'));

    let board = Board {
        board: [
            [None, None, Some('X')],
            [None, Some('O'), Some('X')],
            [Some('O'), None, Some('X')],
        ],
    };

    assert_eq!(board.winner(), Some('X'));

    let board = Board::new();
    assert_eq!(board.winner(), None);
}

#[test]
fn test_terminal() {
    let board = Board::new();
    assert_eq!(board.terminal(), false);

    let board = Board {
        board: [
            [None, None, None],
            [Some('X'), Some('X'), Some('X')],
            [Some('X'), Some('O'), Some('O')],
        ],
    };

    assert_eq!(board.terminal(), true);

    let board = Board {
        board: [
            [Some('X'), Some('X'), Some('O')],
            [Some('O'), Some('O'), Some('X')],
            [Some('X'), Some('O'), Some('X')],
        ],
    };

    assert_eq!(board.terminal(), true);
}

#[test]
fn test_player() {
    let board = Board::new();
    assert_eq!(board.player(), Some('X'));

    let board = Board {
        board: [
            [None, None, None],
            [None, Some('X'), None],
            [None, None, None],
        ],
    };

    assert_eq!(board.player(), Some('O'));

    let board = Board {
        board: [
            [None, None, None],
            [None, None, None],
            [Some('O'), None, None],
        ],
    };

    assert_eq!(board.player(), Some('X'));

    let board = Board {
        board: [
            [Some('X'), None, Some('X')],
            [None, Some('O'), None],
            [None, None, None],
        ],
    };

    assert_eq!(board.player(), Some('O'));

    let board = Board {
        board: [
            [Some('O'), Some('X'), Some('O')],
            [Some('X'), Some('O'), Some('X')],
            [None, None, None],
        ],
    };

    assert_eq!(board.player(), Some('X'));
}

#[test]
fn test_result() {
    let board = Board::new();
    let result = board.result((1, 1));

    let new_board = Board {
        board: [
            [None, None, None],
            [None, Some('X'), None],
            [None, None, None],
        ],
    };

    assert_eq!(new_board, result);
}

#[test]
fn test_utility() {
    let board = Board::new();
    assert_eq!(board.utility(), None);

    let board = Board {
        board: [[Some('X'); 3], [None; 3], [None; 3]],
    };
    assert_eq!(board.utility(), Some(1));

    let board = Board {
        board: [[None; 3], [Some('O'); 3], [None; 3]],
    };
    assert_eq!(board.utility(), Some(-1));

    let board = Board {
        board: [
            [Some('X'), Some('O'), Some('X')],
            [Some('O'), Some('O'), Some('X')],
            [Some('O'), Some('X'), Some('O')],
        ],
    };

    assert_eq!(board.utility(), Some(0))
}

#[test]
fn test_min_value() {
    let board = Board { board: [
        [ Some('X'), Some('O'), Some('X') ],
        [ Some('X'),      Some('O'), None ],
        [ None, None, None ]
    ] };

    assert_eq!(Board::min_value(&board), Some(-1));

    let board = Board::new();
    assert_eq!(Board::min_value(&board), Some(0));

    let board = Board { board: [
        [ Some('X'), Some('O'), Some('X') ],
        [ None,      Some('O'), None ],
        [ None; 3],
    ] };

    assert_eq!(Board::min_value(&board), Some(0));

    let board = Board { board: [
        [ Some('X'), None, None ],
        [ None; 3],
        [ None; 3], ] };

    assert_eq!(Board::min_value(&board), Some(0));

}

#[test]
fn test_max_value() {
    let board = Board { board: [
        [ Some('X'), Some('O'), Some('X') ],
        [ None, Some('O'), None ],
        [ None, None, None ],
    ] };
    assert_eq!(Board::max_value(&board), Some(0));

    let board = Board { board: [
        [ Some('X'), Some('O'), Some('X') ],
        [ None,      Some('O'), None ],
        [ None,      Some('O') , None ],
    ] };

    assert_eq!(Board::max_value(&board), Some(-1));
}

#[test]
fn test_minimax() {
    let board = Board { board: [
        [ Some('X'), Some('O'), Some('X') ],
        [ None,      Some('O'), None ],
        [ None, None, None ]
    ] };

    assert_eq!(board.minimax(), Some((2,1)));


    let board = Board { board: [
        [ Some('X'), Some('O'), Some('X') ],
        [ Some('X'), Some('O'), None ],
        [ None, None, None ],
    ] };

    assert_eq!(board.minimax(), Some((2,1)));

}
