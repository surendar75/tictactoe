use std::cmp::{max, min};
use std::collections::HashMap;
use rand::seq::IteratorRandom;

#[derive(Debug)]
pub struct Board {
    pub board: [[Option<char>; 3]; 3],
}

impl PartialEq for Board {
    fn eq(&self, other: &Self) -> bool {
        for x in 0..3 {
            for y in 0..3 {
                if self.board[x][y] != other.board[x][y] {
                    return false;
                }
            }
        }
        return true;
    }
}

impl Board {
    pub fn new() -> Self {
        Self {
            board: [[None; 3]; 3],
        }
    }

    pub fn terminal(&self) -> bool {
        if self.winner().is_some() {
            // if the player winns
            return true;
        }

        // if the empty cell is there then game is not over
        for rows in self.board {
            for cell in rows {
                if cell.is_none() {
                    return false;
                }
            }
        }
        return true;
    }

    pub fn actions(&self) -> Vec<(usize, usize)> {
        let mut positions: Vec<(usize, usize)> = Vec::new();
        for x in 0..3 {
            for y in 0..3 {
                if self.board[x][y].is_none() {
                    positions.push((x, y));
                }
            }
        }
        positions
    }

    pub fn result(&self, action: (usize, usize)) -> Self {
        let mut new_board = self.board.clone();
        if new_board[action.0][action.1].is_none() {
            new_board[action.0][action.1] = self.player();
            return Self { board: new_board };
        }
        panic!("Invalid Action")
    }

    pub fn winner(&self) -> Option<char> {
        for x in 0..3 {
            // check 3 in a row
            if (self.board[x][0].is_some())
                && self.board[x][0] == self.board[x][1]
                && self.board[x][1] == self.board[x][2]
            {
                return self.board[x][0];
            }

            // check 3 in a column
            if (self.board[0][x].is_some())
                && self.board[0][x] == self.board[1][x]
                && self.board[1][x] == self.board[2][x]
            {
                return self.board[0][x];
            }
        }

        //check diagonals
        if self.board[0][0].is_some()
            && self.board[0][0] == self.board[1][1]
            && self.board[1][1] == self.board[2][2]
        {
            return self.board[0][0];
        }

        if self.board[2][0].is_some()
            && self.board[2][0] == self.board[1][1]
            && self.board[1][1] == self.board[0][2]
        {
            return self.board[2][0];
        }
        return None;
    }

    pub fn player(&self) -> Option<char> {
        let mut x_moves = 0;
        let mut o_moves = 0;

        // O(9) -> O(1)
        for rows in self.board {
            for cell in rows {
                if cell == Some('X') {
                    x_moves += 1;
                }

                if cell == Some('O') {
                    o_moves += 1;
                }
            }
        }

        if x_moves <= o_moves {
            return Some('X');
        }
        return Some('O');
    }

    pub fn utility(&self) -> Option<isize> {
        if self.terminal() {
            return match self.winner() {
                Some('X') => Some(1),
                Some('O') => Some(-1),
                None => Some(0),
                _ => None,
            };
        }
        return None;
    }

    pub fn max_value(other: &Self) -> Option<isize> {
        if Self::terminal(&other) {
            return Self::utility(&other);
        }
        
        let mut value = isize::MIN;
        for action in Self::actions(&other) {
            value = max(value, Self::min_value(&Self::result(&other, action)).unwrap());
        }
        return Some(value);
    }

    pub fn min_value(other: &Self) -> Option<isize> {
        if Self::terminal(&other) {
            return Self::utility(&other);
        }

        let mut value = isize::MAX;
        for action in Self::actions(&other) {
            value = min(value, Self::max_value(&Self::result(&other, action)).unwrap());
        }
        return Some(value);
        
    }
    
    // returns the best action accroding to the current player
    pub fn minimax(&self) -> Option<(usize, usize)> {
        if self.terminal() {
            return None;
        }

        match self.player() {
            Some('X') => {
                let mut best_value = isize::MIN;

                let mut actions: HashMap<(usize, usize), isize> = HashMap::new();
                let mut best_actions: HashMap<(usize, usize), isize> = HashMap::new();

                for action in self.actions() {
                    let result = self.result(action);
                    let value = Self::min_value(&result).unwrap();
                    actions.insert(action, value);

                    if value > best_value {
                        best_value = value;
                    }
                }

                for (action, value) in &actions {
                    if  *value == best_value {
                        best_actions.insert(*action, *value);
                    }
                }
                
                let best_action = best_actions.keys().choose(&mut rand::thread_rng());

                //returns Option<*best_action>
                return best_action.copied();
                
            },

            Some('O') => {
                let mut best_value = isize::MAX;

                let mut actions: HashMap<(usize, usize), isize> = HashMap::new();
                let mut best_actions: HashMap<(usize, usize), isize> = HashMap::new();

                for action in self.actions() {
                    let result = self.result(action);
                    let value = Self::max_value(&result).unwrap();
                    actions.insert(action, value);

                    if value < best_value {
                        best_value = value;
                    }
                }
                
                for (action, value) in &actions {
                    if *value == best_value {
                        best_actions.insert(*action, *value);
                    }
                }
                
                let best_action = best_actions.keys().choose(&mut rand::thread_rng());

                //returns Option<*best_action>
                return best_action.copied();

            },

            _ => return None,
        }
    }

}
