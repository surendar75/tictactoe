use std::io;

use crate::board::Board;

pub fn choose_player() -> Option<char> {

    loop {

        println!("Do you want to play as X or O");
        
        let mut player_choice = String::new();
        io::stdin()
            .read_line(&mut player_choice)
            .expect("Unable to read user input!");

        let player_choice = player_choice.trim();
        if player_choice == 'x'.to_string() || player_choice == 'X'.to_string() {
            return Some('X');
        }
        else if player_choice == 'o'.to_string() || player_choice == 'O'.to_string() {
            return Some('O');
        }

    }

}

pub fn auto_play(state: Board) -> Board {
    let best_action =  state.minimax();
    return state.result(best_action.unwrap());
    // return new board with auto move
}

pub fn new_move(state: Board, new_action: (usize, usize)) -> Board {
    for action in state.actions() {
        if action == new_action {
            return state.result(new_action);
        }
    }
    println!("position occupied");
    return state;
}

