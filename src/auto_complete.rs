// This is to test the minimax algorithm. The game results should alawys tie.
// If any player either X or O won then minimax function is not implemented corretly


use std::{thread, time};


mod board;
mod draw_board;

use board::Board;

fn main()  {
    
    let two_seconds = time::Duration::from_millis(2000);

    let mut new_game = Board::new();
    draw_board::draw_board(&new_game);
    thread::sleep(two_seconds);

    draw_board::clear_terminal(10);
    while new_game.terminal() == false {
        new_game = auto_play(new_game);
        draw_board::draw_board(&new_game);
        thread::sleep(two_seconds);
        draw_board::clear_terminal(10);
    }



}

fn auto_play(state: Board) -> Board {
    let best_action = state.minimax();
    return state.result(best_action.unwrap());
}
