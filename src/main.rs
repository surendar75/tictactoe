use ferris_says::*;
use std::io;
use std::io::{ stdout, BufWriter };
use std::{ thread, time };

// local modules


mod board;
// functions for board

mod draw_board;
// function that are useful to draw board on terminal

mod assistant;
//helper functions

use crate::board::Board;

fn main() {

    let game_mode = loop {
    
        draw_board::clear_terminal(5);
        println!("===x====   Tictactoe Game  ====o===");
        draw_board::clear_terminal(2);

        println!("Select Game Mode:");
        println!("press 1 to play with your friend");
        println!("press 2 to play with computer");

        let mut input = String::new();
    
        io::stdin()
            .read_line(&mut input)
            .expect("Unable to read user input!");

        let input = input.trim();

        let input: usize = match input.parse() {
            Ok(num) => num,
            Err(_) => 100,
        };

        if input == 1 || input == 2 {
            break input;
        }

   };

    let half_second = time::Duration::from_millis(500);

    let mut new_game = Board::new();
    draw_board::draw_board(&new_game);

    // play with other user
    if game_mode == 1 {
        while new_game.terminal() == false {
            draw_board::clear_terminal(5);
            let user_action = ask_input();
            new_game = assistant::new_move(new_game, user_action);
            draw_board::draw_board(&new_game);
            thread::sleep(half_second);
        }
    }

    else {

        let user = assistant::choose_player();
        while new_game.terminal() == false {
        draw_board::clear_terminal(5);
        thread::sleep(half_second);

            // if it user turn
            if new_game.player() == user {
                //ask input
                let user_action = ask_input();
                new_game = assistant::new_move(new_game, user_action);
                draw_board::draw_board(&new_game);
            }

            else {
                new_game = assistant::auto_play(new_game);
                draw_board::draw_board(&new_game);
            }
        }
    }

    draw_board::clear_terminal(5);

    let stdout = stdout();
    let width = 24;
    let writer = BufWriter::new(stdout.lock());

    match new_game.winner() {
        Some('X') => {
            let message = "Player X won";
            say(message, width, writer).unwrap();
        }
        Some('O') => {
            let message = "Player O won";
            say(message, width, writer).unwrap();
        }
        None => {
            let message = "Tough Opponent: Tie";
            say(message, width, writer).unwrap();
        }
        _ => {
            println!();
        }
    }
    draw_board::clear_terminal(5);



}

fn ask_input() -> (usize, usize) {
    loop {
        println!("Enter position(1-9): ");
        let mut cell_pos = String::new();
        io::stdin()
            .read_line(&mut cell_pos)
            .expect("unable to read input");

        let cell_pos: usize = match cell_pos.trim().parse() {
            Ok(number) => number,
            Err(_) => 10,
        };

        if cell_pos > 0 && cell_pos < 10 {
            let x: usize = (cell_pos - 1) / 3;
            let y: usize = (cell_pos - 1) % 3;
            return (x, y);
        }
    }
}
