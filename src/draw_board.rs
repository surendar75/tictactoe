use crate::board::Board;

pub fn draw_board(new_board: &Board) {
    draw_rows(11, 3, new_board.board[0]);
    line(34);
    draw_rows(11, 3, new_board.board[1]);
    line(34);
    draw_rows(11, 3, new_board.board[2]);
    
}

//prints space horizontally
fn space(width: usize) {
    for _ in 0..width {
        print!(" ");
    }
}

// helps to draw the columns with pipe
fn brick(width: usize) {
    space(width);
    print!("|");
}

// helps to place the player on the cell
fn brick_with_shape(width: usize, letter: char) {
    space(width/2);
    print!("{}", letter);
    space(width/2);
    print!("|");
}

// helps to place a player at last column
fn shape(width:usize, letter:char) {
    space(width/2);
    print!("{}", letter);
    space(width/2);
}

// draws line with -
fn line(width: usize) {
    for _ in 0..width {
        print!("-");
    }
    println!();
}

// prints the newlines as specified
pub fn clear_terminal(height: usize) {
    for _ in 0..height {
        println!();
    }
}

// helps to print all three columns
fn cell(width: usize, height: usize) {
    for _ in 0..height {
        for _ in 0..2 {
            brick(width);
        }
        println!();
    }
}

// helps to draw multiple pieces on the board
fn draw_rows(width: usize, height: usize, row: [Option<char>; 3]) {
    match row {
        [ Some(x), Some(y), Some(z) ] => {
            cell(width, height/2);
            brick_with_shape(width, x);
            brick_with_shape(width, y);
            shape(width, z);
            println!();
            cell(width, height/2);
        },

        [ None, None, None ] => {
            cell(width, height);
        },

        [ Some(x), Some(y), None ] => {
            cell(width, height/2);
            brick_with_shape(width, x);
            brick_with_shape(width, y);
            println!();
            cell(width, height/2);
        },

        [ None, Some(x), Some(y) ] => {
            cell(width, height/2);
            brick(width);
            brick_with_shape(width, x);
            shape(width, y);
            println!();
            cell(width, height/2);
        },

        [ Some(x), None, Some(y) ] => {
            cell(width, height/2);
            brick_with_shape(width, x);
            brick(width);
            shape(width, y);
            println!();
            cell(width, height/2);
        },

        [ Some(x), None, None ] => {
            cell(width, height/2);
            brick_with_shape(width, x);
            brick(width);
            println!();
            cell(width, height/2);
        },

        [ None, Some(x), None ] => {
            cell(width, height/2);
            brick(width);
            brick_with_shape(width, x);
            println!();
            cell(width, height/2);
        },

        [ None, None, Some(x) ] => {
            cell(width, height/2);
            brick(width);
            brick(width);
            shape(width, x);
            println!();
            cell(width, height/2);
        },
    };

}
