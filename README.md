# tictactoe

Tic-Tac-Toe game implemented in Rust using minimax algorithm for AI opponent and simple gui using ASCII art.

* TODO ->
    1. To create a better GUI
    2. Alpha-Beta pruning

# Getting Started
  To get local copy and running follow these simple steps

  # prerequisites
  The rustc and cargo package manager

  # to install rust

  https://www.rust-lang.org/tools/install

  # Setting up the Project

  1. git clone https://codeberg.org/surendar75/tictactoe.git # to clone the repo
  2. cd tictactoe/ # change the directory
  3. cargo build  # only once
  4. cargo run    # run the game

# License

Distributed under the MIT LICENSE. See LICENSE for more information
